from src import archive
import math
import pytest

def even(number):
    if number % 2 == 0:
        return True
    else:
        return False

def test_even():
    result = even(10)
    assert result is True


def test_non_even():
    result = even(11)
    assert result is False

@pytest.fixture
def test_func1():
    def func(a, x):
        return a*x**2
    return func

@pytest.fixture
def test_func2():

    def func(a, x):
        return 1 / math.log(a * x + 1)

    return func

def test_simpsons1(test_func1):
    result = archive.simpson(test_func1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_simpsons2(test_func2):
    result = archive.simpson(test_func2, 1, 0.5, 1.9, intervals=1000)
    assert abs(result - 1.94607) <= 0.1


def test_rectangle1(test_func1):
    result = archive.rectangle(test_func1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_rectangle2(test_func2):
    result = archive.rectangle(test_func2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1


def test_trapezoid1(test_func1):
    result = archive.trapezoid(test_func1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_trapezoid2(test_func2):
    result = archive.trapezoid(test_func2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1